### 参考博客

> https://juejin.im/post/5d51feaef265da039005219e#heading-4

### 函数调用

* 当一个表达式为函数接着一个`(`，一些用逗号分隔的参数以及一个`）`时，函数调用被执行。

* 函数调用表达式不能是属性方式的调用，如`obj.myFunc()`,这是方法调用，不是函数调用。

* 立即调用函数也是一种函数表达式

  ```javascript
  const message = (function(name) {
     return 'Hello ' + name + '!';
  })('World');
  ```

* 函数调用中的this

  > this在函数调用中是一个全局对象

this是一个window对象，在函数调用中，执行上下文是全局对象。

```javascript
function sum(a, b) {
   console.log(this === window); // => true
   this.myNumber = 20; // 将'myNumber'属性添加到全局对象
   return a + b;
}
// sum() is invoked as a function
// sum() 中的 `this` 是一个全局对象（window）
sum(15, 16);     // => 31
window.myNumber; // => 20
```

在调用`sum(15,16)`时，JS自动将`this`设置为全局对象，在浏览器中该对象时`window`。

当this在任意函数作用域（最顶层作用域：全局执行上下文）之外使用，`this`表示`window`对象。

```javascript
console.log(this === window); // => true
this.myString = 'Hello World!';
console.log(window.myString); // => 'Hello World!'

<!-- In an html file -->
<script type="text/javascript">
   console.log(this === window); // => true
</script>
```

* this在内部函数调用的坑

  ```javascript
  const numbers = {
     numberA: 5,
     numberB: 10,
     sum: function() {
       console.log(this === numbers); // => true
       function calculate() {
         console.log(this === numbers); // => false
         return this.numberA + this.numberB;
       }
       return calculate();
     }
  };
  numbers.sum(); // => NaN 
  ```

  解析：`sum()`是对象上的方法调用，所以`sum`中的上下文是`number`对象，`calculate()`函数是在`sum`中定义的。`calculate()`是一个函数调用（不是方法调用）,它将`this`作为全局对象`window`。

  修改：

  ```javascript
  const numbers = {
     numberA: 5,
     numberB: 10,
     sum: function() {
       console.log(this === numbers); // => true
       function calculate() {
         console.log(this === numbers); // => true
         return this.numberA + this.numberB;
       }
       // 使用 .call() 方法修改上下文
       return calculate.call(this);
     }
  };
  numbers.sum(); // => 15
  ```

  箭头函数：

  ```javascript
  const numbers = {
     numberA: 5,
     numberB: 10,
     sum: function() {
       console.log(this === numbers); // => true
       const calculate = () => {
         console.log(this === numbers); // => true
         return this.numberA + this.numberB;
       }
       return calculate();
     }
  };
  numbers.sum(); // => 15
  ```

  

  

### 方法调用

